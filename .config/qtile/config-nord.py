# Richard Davis' Qtile config
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
# Following 2 for autostart applications
import os
import subprocess

mod = "mod4"
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.next(), desc="Move focus to next window"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn("rofi -modi 'drun' -show drun"), desc="Run rofi"),

    # My own additions
    Key([mod], "b", lazy.spawn("firefox"), desc="Run Firefox"),
    Key([mod, "shift"], "b", lazy.spawn("brave-browser"), desc="Run Brave"),
    Key([mod], "f", lazy.spawn("pcmanfm"), desc="Run pcmanfm"),
    Key([mod, "shift"], "r", lazy.spawn("reaper"), desc="Run Reaper"),
    Key([mod], "m", lazy.spawn("./Applications/MuseScore.AppImage"), desc="Run MuseScore"),
    Key([mod], "v", lazy.spawn("./Applications/vmpk.AppImage"), desc="Run vmpk"),
    Key([mod, "shift"], "v", lazy.spawn("vlc"), desc="Run vlc"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-"), desc="Lower volume"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+"), desc="Raise volume"),
    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master toggle"), desc="Toggle mute"),
    Key([mod], "l", lazy.screen.next_group(), desc="Move to right group"),
    Key([mod], "h", lazy.screen.prev_group(), desc="Move to left group"),
    Key([], "Print", lazy.spawn("gnome-screenshot --interactive"), desc="Open screenshot utility"),
    Key([mod], "t", lazy.spawn("leafpad"), desc="Open leafpad"),
    Key([mod], "s", lazy.spawn("rofi -show power-menu -modi power-menu:./.config/rofi/scripts/rofi-power-menu -width 12 -lines 6"), desc="Rofi power prompt"),
]

group_names = ["1. www", "2. mus", "3. tty", "4. mtg", "5. doc", "6. med", "7. sys", "8. msc"]

groups = [Group(name) for name in group_names]

for i, (name) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

colors = {
    "foreground": "eceff4",
    "background": "2e3440",
    "focus": "5e81ac",
    "focus2": "88c0d0",
    "urgent": "bf616a",
}

layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": colors["focus"],
    "border_normal": colors["background"],
}

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Columns(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Ubuntu Mono',
    fontsize=13,
    padding=3,
    foreground=colors["foreground"],
    background=colors["background"],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
        widget.CurrentLayoutIcon(),
		widget.Spacer(length=6),
        widget.GroupBox(disable_drag=True,
            highlight_method="line",
            this_current_screen_border=[colors["focus2"], colors["focus2"]],
            highlight_color=[colors["focus"], colors["focus"]],
            inactive=colors["foreground"],
            urgent_border=colors["urgent"],
        ),
		widget.Spacer(length=6),
        widget.WindowName(),
        widget.Chord(
                chords_colors={
                    'launch': ("#ff0000", "#ffffff"),
                },
                name_transform=lambda name: name.upper(),
        ),
		widget.Notify(default_timeout=10),
		widget.Spacer(length=6),
        widget.Clock(format='%A, %d %B %Y, %H:%M'),
		widget.Spacer(length=6),
		widget.YahooWeather(
            coordinates={'latitude': 45.50884, 'longitude': -73.58781},
            format="{location_city}: {current_observation_condition_temperature} °C, {current_observation_condition_text}"
        ),
		widget.Spacer(length=6),
		#widget.TextBox(text="Vol: ", padding=0),
		#widget.PulseVolume(),
		#widget.Spacer(length=6),
        widget.Systray(),
		widget.Spacer(length=6),
        widget.LaunchBar(
            progs=[
                ("[logout]", "qshell:self.qtile.cmd_shutdown()", "logout"),
                ("[suspend]", "systemctl suspend", "suspend"),
                ("[reboot]", "reboot", "reboot"),
                ("[shutdown]", "shutdown now", "shutdown")
            ]
        ),
            ],
	    opacity=0.9,
            size=24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    Match(wm_type='utility'),
    Match(wm_type='notification'),
    Match(wm_type='toolbar'),
    Match(wm_type='splash'),
    Match(wm_type='dialog'),
    Match(wm_class='file_progress'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# My hooks
# Autostart applications in ~/.config/qtile/autostart.sh
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    try:
        subprocess.call([home + '/.config/qtile/autostart-nord.sh'])
    except Exception as e:
        with open('qtile_log', 'a+') as f:
            f.write(str(e) + '\n')

# Sending clients to appropriate groups
@hook.subscribe.client_new
def func(c):
    if c.name == "Zoom" or c.name == "Zoom Meeting" or c.name == "Choose ONE of the audio conference options" or c.name == "Zoom - Free Account" or "meeting" in c.name.lower() or "zoom" in c.name.lower():
        c.togroup("4. mtg") # Spawns Zoom clients in mtg group
    elif "brave" in c.name.lower() or "mozilla firefox" in c.name.lower():
        c.togroup("1. www") # Spawns web browsers in www group
    elif "musescore" in c.name.lower() or "frescobaldi" in c.name.lower():
        c.togroup("2. mus") # Spawns notation software in mus group
    elif "midi" in c.name.lower(): 
        c.togroup("2. mus")
        c.floating = True
    elif "vlc" in c.name.lower() or "inkscape" in c.name.lower():
        c.togroup("6. med") # Spawns VLC and Inkscape in med group
    elif "volume control" in c.name.lower():
        c.togroup("7. sys")
    elif "libreoffice" in c.name.lower(): # Spawns LibreOffice in doc group
        c.togroup("5. doc")
    elif "steam" in c.name.lower(): # Spawns Steam in msc group
        c.togroup("8. msc")
    elif "Screenshot" == c.name: # Sends screenshot to floating
        c.floating = True
    elif "Calculator" == c.name: # Sends calculator to floating
        c.floating = True
    elif "(untitled)" in c.name.lower(): # Poorly spawns leafpad in floating
        c.floating = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
