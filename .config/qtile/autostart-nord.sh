#!/bin/bash
# Programs to autostart with Qtile
# Must run $ chmod +x autostart.sh

xrandr --output DP-4 --mode 1920x1080 --rate 144.00
ibus-daemon &
compton &
redshift &
nm-applet &
parcellite &
volumeicon &
feh --bg-scale ~/Wallpapers/bg-nord.png
