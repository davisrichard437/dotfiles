#!/usr/bin/env bash
# ~/.config/menu.sh
# Clickable menu for XMobar, or any similarly minimal tiling window manager
# Requires dzen2 package

# App names
declare -a AP
AP+=("(MENU)")
AP+=("alacritty")
AP+=("firefox")
AP+=("musescore")
AP+=("pcmanfm")
AP+=("synaptic")
AP+=("vim")
AP+=("vlc")
AP+=("vmpk")
AP+=("suspend")
AP+=("reboot")
AP+=("shutdown")

# Actions:
declare -A AC
AC["alacritty"]="alacritty"
AC["firefox"]="firefox-esr"
AC["musescore"]="~/Applications/MuseScore.sh"
AC["pcmanfm"]="pcmanfm"
AC["synaptic"]="synaptic-pkexec"
AC["vim"]="alacritty -e vim"
AC["vlc"]="vlc"
AC["vmpk"]="~/Applications/vmpk.AppImage"
AC["suspend"]="systemctl suspend"
AC["reboot"]="systemctl reboot"
AC["shutdown"]="systemctl poweroff"

# Font:
FN="-b&h-lucidatypewriter-bold-*-normal-sans-12-120-75-75-m-70-iso8859-1"

# Colors:
FG=#f8f8f2
BG=#282a36

# App choice
check=1
while [ $check -eq 1 ]
do
	ex=$(printf '%s\n' "${AP[@]}" | dzen2 -y 0 -x 6 -fn "$FN" -bg "$BG" -fg "$FG" -l ${#AC[@]} -ta c -sa c -w 88 -h 24 -m -p -e 'onstart=collapse;entertitle=uncollapse;button1=menuprint,exit;enterslave=uncollapse;leaveslave=collapse;key_Escape=collapse;button2=collapse')
	exec ${AC[$ex]} &
done
