filetype off
set runtimepath+=/usr/share/lilypond/2.18.2/vim
filetype on
syntax on

call plug#begin()
Plug 'preservim/NERDTree'
call plug#end()

set mouse=a
set number
set tabstop=4
set foldmethod=marker

func! WordProcessorMode()
	setlocal smartindent
	setlocal spell spelllang=en_us
	setlocal noexpandtab
endfu

com! WP call WordProcessorMode()
