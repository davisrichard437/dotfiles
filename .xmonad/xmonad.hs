-- Richard Davis' XMonad config

-- Imports
import XMonad
import Data.Monoid
import Graphics.X11.ExtraTypes.XF86
import System.Exit
import XMonad.Actions.CycleWS
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- Default terminal
myTerminal      = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth   = 2

-- Sets modkey to windows key
myModMask       = mod4Mask

-- Workspaces:
-- Don't know what's happening here just stole it from DT and it worked.
xmobarEscape = concatMap doubleLts
    where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces = clickable . map xmobarEscape
               $ ["WWW", "MUS", "TTY", "MTG", "DOC", "MED", "SYS", "MSC"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show n ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..8] l,
                      let n = i ]


-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor  = "#282a36"
myFocusedBorderColor = "#bd93f9"

------------------------------------------------------------------------
-- Key bindings:
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- terminal
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)

    -- launch rofi
    , ((modm,               xK_r     ), spawn "rofi -show drun")

    -- close focused window
    , ((modm,               xK_w     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_Tab   ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_Tab ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_space   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_m     ), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Display next workspace
    , ((modm,               xK_l     ), nextWS  )

    -- Display previous workspace
    , ((modm,               xK_h     ), prevWS  )

    -- Shrink the master area
    , ((modm .|. shiftMask, xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm .|. shiftMask, xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- MY ADDITIONS
    , ((modm .|. shiftMask, xK_r     ), spawn "gmrun") -- gmrun
    , ((modm,               xK_b     ), spawn "firefox-esr") -- firefox
    , ((modm .|. shiftMask, xK_b     ), spawn "firefox-esr --private-window") -- private firefox
    , ((modm,               xK_p     ), spawn "synaptic-pkexec") -- synaptic
    , ((modm .|. shiftMask, xK_p     ), spawn "pavucontrol") -- pulse audio volume control
    , ((modm,               xK_f     ), spawn "pcmanfm") -- file manager
    , ((modm,               xK_m     ), spawn "PULSE_LATENCY_MSEC=30 ./Applications/MuseScore.AppImage") -- musescore
    , ((modm,               xK_v     ), spawn "./Applications/vmpk.AppImage") -- virtual midi piano keyboard
    , ((modm .|. shiftMask, xK_v     ), spawn "vlc") -- vlc media player
    , ((0,               xK_Print ), spawn "gnome-screenshot --interactive") -- screenshot utility
    , ((modm,               xK_e     ), spawn "alacritty -e vim") -- opens vim in alacritty
    , ((modm,               xK_s     ), spawn "rofi -show power-menu -modi power-menu:./.config/rofi/scripts/rofi-power-menu -width 12 -lines 6") -- rofi-power-menu

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")]
    ++

    -- mod-[1..8], Switch to workspace N
    -- mod-shift-[1..8], Move client to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_8]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings:
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

------------------------------------------------------------------------
-- Layouts:
myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full)
    where
        tiled = Tall 1 (3/100) (1/2)

------------------------------------------------------------------------
-- Window rules:
myManageHook = composeAll
    [ className =? "Gnome-calculator" --> doFloat -- Calculator to floating
    , className =? "Firefox-esr"      --> doShift ( myWorkspaces !! 0 ) -- Firefox to WWW
    , className =? "MuseScore3"       --> doShift ( myWorkspaces !! 1 ) -- MuseScore to MUS
    , title =? "Virtual MIDI Piano Keyboard"    --> doShift ( myWorkspaces !! 1 ) -- VMPK to MUS, not working
    , title =? "Virtual MIDI Piano Keyboard"    --> doFloat -- VMPK to floating
    , className =? "zoom"             --> doShift ( myWorkspaces !! 3 ) -- Zoom to MTG
    , className =? "libreoffice" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "libreoffice-writer" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "libreoffice-calc" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "libreoffice-math" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "libreoffice-impress" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "libreoffice-draw" --> doShift ( myWorkspaces !! 4 ) -- LibreOffice to DOC
    , className =? "vlc"              --> doShift ( myWorkspaces !! 5 ) -- VLC to MED
    , className =? "kdenlive"         --> doShift ( myWorkspaces !! 5 ) -- Kdenlive to MED
    , className =? "Pavucontrol"      --> doShift ( myWorkspaces !! 6 ) -- pavucontrol to SYS
    , className =? "Synaptic"         --> doShift ( myWorkspaces !! 6 ) -- synaptic to SYS
    , className =? "Steam"            --> doShift ( myWorkspaces !! 7 ) -- Steam to MSC
    , resource  =? "desktop_window"   --> doIgnore
    , resource  =? "kdesktop"         --> doIgnore 
    , isFullscreen --> doFullFloat ] -- helps handle fullscreen events

------------------------------------------------------------------------
-- Event handling:
myEventHook = mempty

------------------------------------------------------------------------
-- Startup hook:
myStartupHook = do
    spawnOnce "/usr/lib/geoclue-2.0/demos/agent &"
    spawnOnce "xrandr --output DP-4 --mode 1920x1080 --rate 144.00"
    spawnOnce "light-locker --lock-after-screensaver 10 --lock-on-suspend"
    spawnOnce "~/.config/menu.sh &"
    spawnOnce "lxpolkit &"
    spawnOnce "stalonetray &"
    spawnOnce "ibus-daemon &"
    spawnOnce "nm-applet &"
    spawnOnce "parcellite &"
    spawnOnce "volumeicon &"
    spawnOnce "compton &"
    spawnOnce "redshift &"
    spawnOnce "feh --bg-scale ~/Wallpapers/bg-dracula.png"

------------------------------------------------------------------------
-- XMonad main:
main = do
    xmproc <- spawnPipe "xmobar ~/.config/xmobar/xmobarrc"
    xmonad $ ewmh $ docks defaultConfig
        {
            -- simple stuff
            terminal           = myTerminal,
            focusFollowsMouse  = myFocusFollowsMouse,
            clickJustFocuses   = myClickJustFocuses,
            borderWidth        = myBorderWidth,
            modMask            = myModMask,
            workspaces         = myWorkspaces,
            normalBorderColor  = myNormalBorderColor,
            focusedBorderColor = myFocusedBorderColor,

          -- key bindings
            keys               = myKeys,
            mouseBindings      = myMouseBindings,

          -- hooks, layouts
            layoutHook         = spacingRaw False (Border 0 6 0 6) True (Border 6 0 6 0) True $ smartBorders myLayout,
            manageHook         = myManageHook,
            handleEventHook    = myEventHook <+> fullscreenEventHook,
            logHook            = dynamicLogWithPP xmobarPP
                { ppOutput = \x -> hPutStrLn xmproc x
                , ppCurrent = xmobarColor "#bd93f9" "" . wrap "[" "]"
                , ppTitle = xmobarColor "#f8f8f2" "" . shorten 50
                , ppHiddenNoWindows = xmobarColor "#f8f8f2" "" . shorten 50
                , ppVisible = xmobarColor "#f8f8f2" "" . shorten 50
                , ppUrgent = xmobarColor "#ff5555" "" . wrap "!" "!"
                , ppSep = "<fc=#f8f8f2> | </fc>"
                },
            startupHook        = myStartupHook
        }
