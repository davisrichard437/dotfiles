# Richard Davis's Dotfiles

These are Richard Davis's dotfiles for a Linux configuration. Configurations are currently available for awesome wm (bad, outdated), qtile (better), and XMonad (work in progress). 

I strongly advise against cloning this repo. The code here will be terrible. I promise.

## Dependencies

### Autostart programs

#### Qtile

- feh
- compton
- redshift
- nm-applet (from nm-tray)
- parcellite
- ibus-daemon (from ibus)

#### XMonad

- light-locker
- lxpolkit
- stalonetray
- ibus-daemon
- nm-applet
- parcellite
- volumeicon
- compton
- redshift
- feh

### Launch keybindings

- rofi
- rofi-power-menu ([github](https://github.com/jluttine/rofi-power-menu), slightly modified for my purposes)
- alacritty
- firefox
- brave-browser
- pcmanfm
- MuseScore (AppImage)
- VMPK (AppImage)
- Reaper ([download](https://www.reaper.fm/download.php#linux_download))
- pavucontrol
- gnome-screenshot

### Other

- lynx
- vim
	- Package manager: [vim-plug](https://github.com/junegunn/vim-plug)
	- Plugins: [NERDTree](https://github.com/preservim/nerdtree)

### GTK

- Icon Theme: [Tela-purple-dark](https://www.gnome-look.org/s/Gnome/p/1279924)
- GTK Theme: [Dracula](https://draculatheme.com/gtk/)

## Screenshots:

### Qtile

![qtile-dracula-2](/Screenshots/qtile-dracula-2.png)

### XMonad

![xmonad-dracula](/Screenshots/xmonad-dracula.png)
